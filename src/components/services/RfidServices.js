import Api from './Api'

export default {
  getRfidUsers (page, search) {
    return Api().get(`rfid-users?page=${page}&search=${search}`)
  },
  getRfid (id) {
    return Api().get(`rfid-users/edit/${id}`)
  },
  saveRfid (user) {
    return Api().post('rfid-users', user)
  },
  updateRfid (user, id) {
    return Api().put(`rfid-users/${id}`, user)
  },
  deleteRfid (id) {
    return Api().delete(`rfid-users/${id}`)
  }
}