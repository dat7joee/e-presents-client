import Api from './Api'

export default {
    getTokenUsers(page, search){
      return Api().get(`users-token?page=${page}&search=${search}`)
    },
    getAksiToken(){
      return Api().get('users-token/aksi')
    },
    getDosenInval (id) {
      return Api().get(`users-token/dosen-inval/${id}`)
    },
    getRfidData(id){
      return Api().get('rfid-users/'+id)
    },
    addTokenUser(user){
      return Api().post('users-token', user)
    },
    deleteTokenUser(id){
      return Api().delete('users-token/'+id)
    },
    cariDosen (search) {
      return Api().get(`dosen?search=${search}`)
    },
    cariOrang (keywords) {
      return Api().get(`cari?keywords=${keywords}`)
    }
}