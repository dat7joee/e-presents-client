import Api from './Api'

export default {
    getBlacklistUsers(page, search) {
        return Api().get(`rfid-blacklist?page=${page}&search=${search}`)
    },
    newBlacklistUser(user) {
        return Api().post('rfid-blacklist', user)
    },
    deleteUser(id) {
        return Api().delete(`rfid-blacklist/${id}`)
    }
}