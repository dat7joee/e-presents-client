import axios from "axios";
import store from '../../store/store'

export default () => {
    return axios.create({
        baseURL: 'http://localhost/e-presents/api/v1/',
        headers: {
            'X-Requested-With': 'XMLHttpRequest',
            'Authorization': `Bearer ${store.state.token_ep}`
        }
    })
}

// Saat ini menggunakan e-presents folder
// Untuk menggunakan e-presents-v2 tambahkan e-presents-v2/public/