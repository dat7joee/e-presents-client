import Api from './Api'

export default {
  getOperators (page, search) {
    return Api().get(`operators?page=${page}&search=${search}`)
  },
  newOperator (user) {
    return Api().post('operators/register', user)
  },
  deleteOperator (id) {
    return Api().delete(`operators/${id}`)
  }
}