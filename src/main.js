import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store/store'
import {sync} from 'vuex-router-sync'
import Notifications from 'vue-notification'
import VueProgressbar from 'vue-progressbar'

const options = {
  color: '#bffaf3',
  failedColor: '#874b4b',
  thickness: '5px',
  transition: {
    speed: '0.2s',
    opacity: '0.8s',
    termination: 300
  },
  location: 'top',
  inverse: false
}

Vue.use(VueProgressbar, options)
Vue.use(Notifications)

sync(store, router)

router.beforeEach((to, some, next) => {
  if(to.matched.some(record => record.meta.Authenticated)){
    if(!store.state.token_ep){
      next({
        path: '/login'
      })
    } else {
      next()
    }
  } else {
    next()
  }
})

new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
})
