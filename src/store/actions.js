import axios from 'axios'

export default {
    deleteRfid({commit, state}, payload){
        setTimeout(() => {
            return new Promise((resolve, reject) => {
                axios.delete(state.server+'rfid-users/'+payload.rf_id+'?token='+payload.token)
                .then((res) => {
                    commit('RFID_DELETED')
                    resolve(res)
                })
                .catch((err) => {
                    reject(err)
                    commit('RFID_FAIL_DELETED')
                })
            }) 
        }, 1000)
    },
    setToken({commit}, token){
        commit('setToken', token)
    },
    setUser({commit}, user){
        commit('setUser', user)
    },
    notSuperAdmin({commit}) {
        console.log('NOT SUPER ADMIN!')
    },
    showMessage ({commit}, payload) {
        commit('showMessage', payload)
    }
}