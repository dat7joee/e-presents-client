import swal from 'sweetalert2'

export default {
    setToken(state, token){
        state.token_ep = token
        if(token){
            state.loggedIn = true
        } else {
            state.loggedIn = false
        }
    },
    setUser(state, user){
        state.user_ep = user
    },
    RFID_DELETED(){
        console.log('DELETED')
    },
    RFID_FAIL_DELETED(){
        console.log('FAILED DELETED')
    },
    showMessage (state, payload) {
        swal({
            ...payload
        })
    }
}