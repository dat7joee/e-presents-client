import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from 'vuex-persistedstate'

import getters from './getters'
import mutations from './mutations'
import actions from './actions'

Vue.use(Vuex)

export default new Vuex.Store({
    plugins: [createPersistedState()],
    state: {
        loggedIn: false,
        token_ep: '',
        user_ep: {}
    },
    getters: getters,
    mutations: mutations,
    actions: actions
})