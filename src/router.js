import Vue from 'vue'
import VueRouter from 'vue-router'
import store from './store/store'

// Components
const Dashboard = () => System.import('./components/Dashboard.vue')
const Rfid = () => System.import('./components/Rfid.vue')
const EditRfid = () => System.import('./components/FormComponents/EditRfid.vue')
const TokenCard = () => System.import('./components/Token.vue')
const Login = () => System.import('./components/login.vue')
const Blacklisted = () => System.import('./components/Blacklisted.vue')
const Operators = () => System.import('./components/Operators.vue')
const NotFound = () => System.import('./components/NotFound.vue')

Vue.use(VueRouter)

const routes = [
    {
        path: '', component: Dashboard, name: 'dashboard', meta: 
        {
            Authenticated: true,
            progress: {
                func: [
                    {call: 'color', modifier: 'temp', argument: '#56a8ff'},
                    {call: 'fail', modifier: 'temp', argument: '#6e0000'},
                    {call: 'location', modifier: 'temp', argument: 'top'},
                    {call: 'transition', modifier: 'temp', argument: {speed: '1.5s', opacity: '0.6s', termination: 400}}
                ]
            }
        }
    },
    {path: '/rfid', component: Rfid, name:'rfid', meta: {
        Authenticated: true,
        progress: {
                func: [
                    {call: 'color', modifier: 'temp', argument: '#56a8ff'},
                    {call: 'fail', modifier: 'temp', argument: '#6e0000'},
                    {call: 'location', modifier: 'temp', argument: 'top'},
                    {call: 'transition', modifier: 'temp', argument: {speed: '1.5s', opacity: '0.6s', termination: 400}}
                ]
            }
        }
    },
    {path: '/rfid/:rfid', component: EditRfid, name:'edit-rfid', meta: {
        Authenticated: true,
        progress: {
                func: [
                    {call: 'color', modifier: 'temp', argument: '#56a8ff'},
                    {call: 'fail', modifier: 'temp', argument: '#6e0000'},
                    {call: 'location', modifier: 'temp', argument: 'top'},
                    {call: 'transition', modifier: 'temp', argument: {speed: '1.5s', opacity: '0.6s', termination: 400}}
                ]
            }
        }
    },
    {
        path: '/token', component: TokenCard,name: 'token', meta: {
        Authenticated: true,
        progress: {
                func: [
                    {call: 'color', modifier: 'temp', argument: '#56a8ff'},
                    {call: 'fail', modifier: 'temp', argument: '#6e0000'},
                    {call: 'location', modifier: 'temp', argument: 'top'},
                    {call: 'transition', modifier: 'temp', argument: {speed: '1.5s', opacity: '0.6s', termination: 400}}
                ]
            }
        }
    },
    {
        path: '/rfid-blacklisted', component: Blacklisted,name: 'rfid-blacklist', meta: {
        Authenticated: true,
        progress: {
                func: [
                    {call: 'color', modifier: 'temp', argument: '#56a8ff'},
                    {call: 'fail', modifier: 'temp', argument: '#6e0000'},
                    {call: 'location', modifier: 'temp', argument: 'top'},
                    {call: 'transition', modifier: 'temp', argument: {speed: '1.5s', opacity: '0.6s', termination: 400}}
                ]
            }
        }
    },
    {
        path: '/operators', component: Operators,name: 'operators',
        meta: {
            Authenticated: true,
            progress: {
                    func: [
                        {call: 'color', modifier: 'temp', argument: '#56a8ff'},
                        {call: 'fail', modifier: 'temp', argument: '#6e0000'},
                        {call: 'location', modifier: 'temp', argument: 'top'},
                        {call: 'transition', modifier: 'temp', argument: {speed: '1.5s', opacity: '0.6s', termination: 400}}
                    ]
            }
        }
    },
    {
        path: '/login', 
        component: Login, 
        beforeEnter: (to, from, next) => {
            if(store.state.token_ep){
                next({
                    name: 'dashboard'
                })
            } else {
                next()
            }
        }
    },
    {path: '*', component: NotFound, meta: {Authenticated: true}}
]

export default new VueRouter({
    mode: 'hash',
    routes
})